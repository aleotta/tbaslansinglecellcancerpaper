#!/bin/bash


arrayname=( CTB9552 CTB9554 CTB9557 CTB9600 CTB9601 CTB9602 CTB9612 CTB9614 CTB9617 )
ext=( .hg19.50k.k50.varbin.data.txt .hg19.50k.k50.nobad.varbin.data.txt )
dir="/mnt/wigclust6/data/safe/tbaslan/GLOBAL/processed/"

for m in "${arrayname[@]}"
do
  for e in "${ext[@]}"
  do
     src_filename=${m}${e}
     dst_filename=${dir}${m}${e}

     if [ -f $src_filename ];
     then
        echo "File $src_filename exists."
     else
        echo "File $src_filename does not exist."
        cp ${dst_filename} .
     fi
  done
done

#for i in $( ls ); do
#    echo item: $i
#done
